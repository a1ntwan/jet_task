# Project for Jet

This script checks CPU, RAM and Disk utilization every 5 seconds and sends it to /var/log/util_log.log by default. 
If any is 80% or more, it alarms to Telegram chat. [Link to chat](https://t.me/+OkNk-6c-ZB5mMDUx)  

**Python version (Python3.7+ required)**:

Run `python3 python_script.py --help` to see the details.

Required:
* `--tg-api-token` Put here telegram token you get from Botfather. [HOWTO](https://core.telegram.org/bots#6-botfather)
* `--tg-chat-id` Put here chat id, you can find it here: **api.telegram.org/bot\<YOUR TG API TOKEN>/getUpdates**

Optional:
* `--log-path` Change the default log path

**Bash version**:

Required:

* `$1` Put here telegram token you get from Botfather. [HOWTO](https://core.telegram.org/bots#6-botfather)
* `$2` Put here chat id, you can find it here: **api.telegram.org/bot\<YOUR TG API TOKEN>/getUpdates**

Optional:
* Reassign **$LOGPATH** variable in **jet_task.sh** to change the default log path

**Steps**:
1. Clone the repo and checkout to the branch you like more;
2. Install the requirements: `python3 -m pip install -r requirements.txt` **(Python3 version only)** or `yum/apt install sysstat -y` **(Bash version only)** 
3. Make jet_task.sh executable: `chmod +x jet_task.sh` **(Bash version only)**
4. Don't forget to put the **required** arguments in **jet_task_python.service(Python3 version)** or **jet_task.service(Bash version)**;
5. Move .service and .timer unit files to **/etc/systemd/system/**;
6. Reinit units by running `systemctl daemon-reload`;
7. Run **jet_task_python.timer (Python3 version)**: `systemctl enable jet_task_python.timer --now`;
8. Run **jet_task.timer (Bash version)**: `systemctl enable jet_task.timer --now`
