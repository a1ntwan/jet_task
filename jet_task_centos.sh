#!/bin/bash

PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
SHELL="/bin/bash"
LC_TIME="en_US.utf8"
API_TOKEN="$1"
CHAT_ID="$2"
LOGPATH="/var/log/util_log.log"

cpu="$(iostat -c 1 2 | sed -n 7p | tr "," "." | awk '{print 100 - ($6 + $5)}')"
ram="$(free -b | sed -n 2p | awk '{printf "%.0f \n", (($2 - $4) / $2 * 100)}')"
disk="$(df -h | grep "/$" | cut -f1 -d "%" | awk '{print $5}')"
timer="$(date +'%d/%m/%y  %T %Z')"
host="$(hostname -f)"

if [ ! -f "$LOGPATH" ]; then
  touch "$LOGPATH"
  printf "%s  CPU util%%: %s | RAM util%%: %s | Disk util%%: %s \n" "$timer" "$cpu" "$ram" "$disk" >> "$LOGPATH" 2>&1
else
  printf "%s  CPU util%%: %s | RAM util%%: %s | Disk util%%: %s \n" "$timer" "$cpu" "$ram" "$disk" >> "$LOGPATH" 2>&1
fi


if [[ "$cpu" -ge 80 || "$ram" -ge 80 || "$disk" -ge 80 ]]; then
  msg="$(printf "%s  CPU%%: %s | RAM%%: %s | Disk%%: %s %%0A Your host: %s is on fire !!! \xF0\x9F\x94\xA5  \n" "$timer" "$cpu" "$ram" "$disk" "$host")"
  curl -s "https://api.telegram.org/bot$API_TOKEN/sendMessage?chat_id=$CHAT_ID&text=$msg"; echo
fi
